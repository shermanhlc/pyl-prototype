// method 1:
// creates a hello world web page at 127.0.0.1:8000/

// #[macro_use] extern crate rocket;

// #[get("/")]
// fn index() -> &'static str {
//     "Hello, world!"
// }

// #[launch]
// fn rocket() -> _ {
//     rocket::build().mount("/", routes![index])
// }

// method 2:
// creates a hello world web page at 127.0.0.1:8000/hellow/world

// #[macro_use] extern crate rocket;

// #[get("/world")]
// fn world() -> &'static str {
//     "Hello, world!"
// }

// #[launch]
// fn rocket() -> _ {
//     rocket::build().mount("/hello", routes![world])
// }

mod database;

use database::*;
use uuid::Uuid;
use rand::Rng;
use rocket::{form::Form, response::content::RawHtml};
//use sqlx::Database;

#[macro_use] extern crate rocket;

// launch annotation designates this as the "main" function
#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![display_create_user, upload_credentials, display_login, check_credentials])
}

#[derive(FromForm)]
struct Credentials {
    pub username: String,
    pub password: String,
}

#[post("/join", data = "<creds>")]
async fn upload_credentials(creds: Form<Credentials>) {
    println!("matt");
    println!("username: {}, password: {}", creds.username, creds.password);

    // TODO: compare password and repassword
    // if password and repassword match, do original code
    // else display an error
    let id = Uuid::new_v4();
    let id_string = id.to_string();
    println!("uuid: {}", id_string);

    ////let random_str = rand::thread_rng().sample_iter(&Alphanumeric).take(16).map(char::from).collect();

    let random_arr: [u8; 16] = {
        let mut rng = rand::thread_rng();
        let mut u8_array: [u8; 16] = [0; 16];
        rng.fill(&mut u8_array);
        u8_array
    };

    println!("salt: {:#?}", random_arr);
    let db = DatabaseCredentials {
        user_id: id_string,
        username: creds.username.clone(),
        password_hash: creds.password.clone(),
        salt: random_arr
    };

    insert(db).await.unwrap();
}

#[get("/join")]
fn display_create_user() -> RawHtml<&'static str> {
    // names for the input types need to line up with the variable names within the struct

    // TODO: add second box to confirm password
    // <input type="password" name="repassword" placeholder="re-enter password">
    RawHtml(r#"
    <form action="/join" method="post">
        <input type="text" name="username" placeholder="username"> 
        <input type="password" name="password" placeholder="password">
        
        <input type="submit" name="submit_button" value="create account">
    </form>
    "#)
}

#[post("/login", data = "<creds>")]
async fn check_credentials(creds: Form<Credentials>) {
    let empty_arr: [u8; 16] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    let db_credentials = DatabaseCredentials {
        user_id: "test".into(),
        username: creds.username.clone(),
        password_hash: creds.password.clone(),
        salt: empty_arr
    };

    grab(db_credentials).await.unwrap();
    // TODO: call grab, compare values
}

#[get("/login")]
fn display_login() -> RawHtml<&'static str> {
    RawHtml(r#"
    <form action="/login" method="post">
        <input type="text" name="username" placeholder="username">
        <input type="password" name="password" placeholder="password">
        <input type="submit" name="submit_buttom" value="login button">
    </form>
    "#)
}



