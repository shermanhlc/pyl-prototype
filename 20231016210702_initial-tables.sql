-- Add migration script here
create table users (
    user_id text primary key,    
    username text unique not null,
    password_hash text not null, -- should be a hash, text for now (now is a hash)
    salt BYTEA not null -- in theory should be unique, but not now for simplicity (now is randomly generated)
);