use async_once::AsyncOnce;
use sqlx::{postgres::PgPoolOptions, QueryBuilder, PgPool, Postgres, FromRow};
use bcrypt::{DEFAULT_COST, hash_with_salt};

#[derive(FromRow)]
pub struct DatabaseCredentials {
    pub user_id: String,
    pub username: String,
    pub password_hash: String,
    pub salt: [u8; 16]
}

lazy_static::lazy_static! {
    static ref POOL: AsyncOnce<PgPool> = async_once::AsyncOnce::new(async {
        dotenv::dotenv().expect("Unable to load .env file");
        let db_url = std::env::var("DATABASE_URL").expect("Unable to read DATABASE_URL");

        let pool = PgPoolOptions::new()
            .max_connections(100)
            .connect(&db_url)
            .await.expect("Unable to connect to Postgres");

            pool
    });
}

// -> indicates return type
// Result indicates an operation that could fail, allows us to print the error somewhere else
pub async fn insert(item: DatabaseCredentials) -> std::result::Result<(), sqlx::Error> {
    // add user_id to beginning of insert into users(<>...

    // TODO: hash password
    let hashed = hash_with_salt(item.password_hash, DEFAULT_COST, item.salt).unwrap().format_for_version(bcrypt::Version::TwoB);
    println!("hashed password: {}", hashed);

    let mut query_builder: QueryBuilder<Postgres> = QueryBuilder::new("insert into users(user_id, username, password_hash, salt) values (");
    let mut separated = query_builder.separated(",");
    separated.push_bind(item.user_id)
        .push_bind(item.username)
        .push_bind(hashed)
        .push_bind(item.salt);
    query_builder.push(");");

    query_builder.build().execute(POOL.get().await).await?;

    return Ok(());
}

pub async fn grab(item: DatabaseCredentials) -> std::result::Result<bool, sqlx::Error> {
    let mut query_builder: QueryBuilder<Postgres> = QueryBuilder::new("SELECT * FROM users WHERE username=");
    
    // binds the username from passed in item
    // somehow knows to "find" that user from the db table
    query_builder.push_bind(&item.username);
    
    // finds the user with the username binded above, then returns a DatabaseCredentials with the values from the table
    let fetched_result = query_builder.build_query_as::<DatabaseCredentials>().fetch_optional(POOL.get().await).await?;
    if let Some(creds) = fetched_result {
        let un = creds.username;
        println!("db username: {}", un);

        let potential_password = hash_with_salt(item.password_hash, DEFAULT_COST, creds.salt).unwrap().format_for_version(bcrypt::Version::TwoB);

        if potential_password == creds.password_hash {
            println!("logged in");
        }
        else {
            println!("wrong username or password (password)");
            return Ok(false);
        }
    } else {
        println!("wrong username or password (username)");
        return Ok(false);
    }


    ////////
    // println!("passed");

    // if fetched_result.username.is_empty() {
    //     println!("successful null");
    // }
    ////////
    
    /////
    // match fetched_result {
    //     Ok(result) => {
    //         // gets username from the result
    //         let un = fetched_result.username;
    //         println!("db username: {:#?}", un);
    //     }
        
    //     Err(error) => {
    //         eprintln!("Error fetching data: {:?}", error);
    //     }
    //}
    /////
    return Ok(true);
}